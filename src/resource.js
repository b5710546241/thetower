var res = {
	town_png: 'res/images/town.png',
	brickwall_png: 'res/images/brick.png',
	whitebg_png: 'res/images/whitebg.png',
	press_wav: 'res/sounds/QUICKPOP.WAV',
	backgroundSound_wav: 'res/sounds/backgroundSound.wav'

};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}