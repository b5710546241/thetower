var Brick = cc.Sprite.extend({
	ctor: function( percentWidth ) {
		this._super();
		this.initWithFile( 'res/images/brick.png' );
		this.direction = Brick.DIR.RIGHT;
        this.presentWidth = percentWidth;
        this.cutWidth( percentWidth );
	},

	update: function( dt ) {
		var pos = this.getPosition();

		if ( pos.x <= 600 && this.direction == Brick.DIR.RIGHT ) 
			 this.setPosition( new cc.Point( pos.x + Brick.SPEED, pos.y ) );
		

		else if ( pos.x >= 200 && this.direction == Brick.DIR.LEFT ) 
		    this.setPosition( new cc.Point( pos.x - Brick.SPEED, pos.y ) );
		

		if ( pos.x >= 600 && this.direction == Brick.DIR.RIGHT ) 
			this.switchDirection();
		 

		else if ( pos.x <= 200 && this.direction == Brick.DIR.LEFT ) 
			this.switchDirection();

    },

    stop: function() {
 		this.unscheduleUpdate();
        
 		
    },

    switchDirection: function() {
    	if ( this.direction == Brick.DIR.LEFT ) 
    		this.direction = Brick.DIR.RIGHT;

    	else 
    		this.direction = Brick.DIR.LEFT; 

    },

    getNewWidth: function( brick ) {
    	var thisPos = this.getPosition();
        var brickPos = brick.getPosition();
        var lastWidth = Brick.STATUS.WIDTH;
        var cutX = ( Math.abs( brickPos.x - thisPos.x )/lastWidth );
        var temp = Math.abs( this.presentWidth - cutX );

        return temp;
    },

    getWidth: function(){
    	return this.presentWidth*Brick.STATUS.WIDTH;
    },

    setWidth: function( width ){
    	this.presentWidth = width;
    },

    cutWidth: function( width ){
        this.presentWidth = width;
        this.setScaleX( width );
    }

});


Brick.DIR = {
	LEFT: 1,
	RIGHT: 2
};

Brick.STATUS = {
	WIDTH : 400,
	HEIGHT : 58
};

Brick.SPEED = 5;
Brick.XPOSITION = 250;
Brick.YPOSITION = 92;


