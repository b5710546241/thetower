var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );

        cc.audioEngine.playMusic( res.backgroundSound_wav, true );

        this.townbg = new cc.Sprite.create( res.town_png );
        this.townbg.setPosition( new cc.Point( screenWidth/2, screenHeight/2 ) );
        this.addChild( this.townbg ); 

        this.baseBrick = new Brick( 1 );
        this.baseBrick.setPosition( new cc.Point( 400, 34 ) );
        this.addChild( this.baseBrick,10 );

        this.baseBrickWidth = 1;
 
        this.brick = new Brick( this.baseBrickWidth );
        this.brick.setPosition( new cc.Point( Brick.XPOSITION, Brick.YPOSITION ) );
        this.addChild( this.brick,10 );
        this.brick.scheduleUpdate();
        this.addKeyBoardHandlers();

        this.scheduleUpdate();

        this.brickArray = [];
        this.brickArray.push( this.baseBrick );
        this.brickArray.push( this.brick );

        this.bgArray = [];
        this.whitebg = new cc.Sprite.create( 'res/images/whitebg.png' );
        this.whitebg.setPosition( new cc.Point( screenWidth/2, screenHeight/2 + screenHeight ) );
        this.addChild( this.whitebg );
        this.bgArray.push( this.whitebg );

        this.whitebg = new cc.Sprite.create( 'res/images/whitebg.png' );
        this.whitebg.setPosition( new cc.Point( screenWidth/2, screenHeight/2 + screenHeight*2 ) );
        this.addChild( this.whitebg );
        this.bgArray.push( this.whitebg );

        this.label = cc.LabelTTF.create( 'HACKED', 'Tahoma', 40 );
        this.label.setPosition( new cc.Point(400, 300) );
        this.addChild( this.label );

        this.countBG = 1;

        return true;
    },

    update: function(){
        var pos = this.getPosition();
        if( this.brick.getPositionY() < -1*this.getPositionY() ) {
                console.log( 'End Game' );
                cc.audioEngine.stopMusic( res.backgroundSound_wav );
                this.unscheduleUpdate();
                this.brick.stop();
                return ;
        }

        if( -1*pos.y >= this.countBG*screenHeight ) {
            this.whitebg = new cc.Sprite.create( 'res/images/whitebg.png' );
            this.whitebg.setPosition( new cc.Point( screenWidth/2, screenHeight/2 + screenHeight*(this.countBG+2) ) );
            this.addChild( this.whitebg );
            this.bgArray.push( this.whitebg );

            this.countBG++;
        }

        this.setPosition( new cc.Point( pos.x,pos.y-( ( this.brickArray.length-2 )*0.07 ) ) );

    },

    onKeyDown: function( keyCode, event ) {
        if ( keyCode == cc.KEY.space ) {
            this.brick.stop();
        }
    },

    onKeyUp: function( keyCode, event ) {
        if ( keyCode == cc.KEY.space ) {
            this.pressSound = new cc.audioEngine.playMusic( res.press_wav );
            Brick.YPOSITION += Brick.STATUS.HEIGHT;
            console.log('up');
            if( this.isEndGame() ) {
                console.log( 'End Game' );
                cc.audioEngine.stopMusic( res.backgroundSound_wav );
                this.unscheduleUpdate();
                return ;
            }

            if( Math.abs( this.brick.getPositionX()-this.baseBrick.getPositionX() ) < 5 ) {
                console.log( "Perfect" );

                var newWidth = this.baseBrick.presentWidth;
                var baseBrickPos = this.baseBrick.getPosition();

                this.brick.setPositionX( baseBrickPos.x );
                this.brick.cutWidth( newWidth );
                this.baseBrick = this.brick;
                
                this.brick = new Brick( newWidth );
                this.brick.setPosition( new cc.Point( Brick.XPOSITION, Brick.YPOSITION ) );
                this.addChild( this.brick, 10 );
                this.brick.scheduleUpdate();
                this.brickArray.push( this.brick );
                
            }
            else {
                var newWidth = this.brick.getNewWidth(this.baseBrick);
                var baseBrickPos = this.baseBrick.getPosition();

                this.brick.setPositionX( this.findBasePosition() );
                this.brick.cutWidth( newWidth );
                this.baseBrick = this.brick;
                
                this.brick = new Brick( newWidth );
                this.brick.setPosition( new cc.Point( Brick.XPOSITION, Brick.YPOSITION ) );
                this.addChild( this.brick,10 );
                this.brick.scheduleUpdate();
                this.brickArray.push( this.brick );
            }
        }
    },

    addKeyBoardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,

            onKeyPressed: function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },

            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }

        }, this);
    },

    findBasePosition: function() {
        var brickPosX = this.brick.getPositionX();
        var baseBrickPosX = this.baseBrick.getPositionX();
        var direction = brickPosX - baseBrickPosX;
        return baseBrickPosX + direction/2;
    },

    isEndGame: function() {
        var brickPosX = this.brick.getPositionX();
        var baseBrickPosX = this.baseBrick.getPositionX();
        var baseBrickWidth = this.baseBrick.getWidth();
        if( this.brick.getWidth() <= 3 ) 
            return true;
        
        if( Math.abs( baseBrickPosX-brickPosX ) >= baseBrickWidth )
            return true;

        return false;
    },

    setBrick: function( baseBrickPos ) {
        var brickPos = this.brick.getPosition();
        this.brick.setPosition( baseBrickPos.x, brickPos.y );
    }

});
 
var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );
    }
});